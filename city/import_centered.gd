tool # needed so it runs in the editor
extends EditorScenePostImport

func post_import(scene):
	for child in (scene as Node).get_children():
		if child is Spatial:
			child.translation = Vector3()
  return scene # remember to return the imported scene
