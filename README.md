# Dejavu of a thief

A cursed thief travels back in time. He is figuring out why. A game for Godot Wild jam 20

Setup:
- Godot v3.2.1
- Git
- Git LFS

Get started:
- Clone the project
- Execute `git lfs fetch && git lfs checkout`
- Open Godot and import the file project.godot