tool
extends Spatial

var GROUPS = {"PLAYER": "player"}
enum STATES {AWAKE, NOCKED}
var state = STATES.AWAKE
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


func _physics_process(delta: float) -> void:
	if not Engine.editor_hint:
		if state == STATES.AWAKE:
			var collider = $RayCast.get_collider()
			if collider != null:
				if collider.is_in_group(GROUPS.PLAYER):
					if !collider.is_hidden():
						EventsMgr.emit_signal('play_requested', 'Police', 'Alerted')
						get_tree().call_group("game_manager","game_over")

func nock_out():
	if state != STATES.NOCKED:
		state = STATES.NOCKED
		$AnimationPlayer.play("Nocked")
		EventsMgr.emit_signal('play_requested', 'Police', 'Knocked')
		$VisionIndicator.visible = false
	

func _on_Policeman_body_entered(body: Node) -> void:
	if body.is_in_group("player"):
		nock_out()


enum RANGES {SHORT, LARGE}
export(RANGES) var vision_range = RANGES.LARGE
var previous_range = vision_range

func change_range():
	match previous_range:
		RANGES.LARGE:
			vision_range = RANGES.SHORT
			$VisionIndicator/MeshInstance2.visible = false
			$VisionIndicator/MeshInstance.scale.z = 1
			$VisionIndicator/MeshInstance.translation.z = 1
			$RayCast.cast_to.z = 2
		RANGES.SHORT:
			vision_range = RANGES.LARGE
			$VisionIndicator/MeshInstance2.visible = true
			$VisionIndicator/MeshInstance.scale.z = 2
			$VisionIndicator/MeshInstance.translation.z = 2
			$RayCast.cast_to.z = 4

func _process(delta):
	if vision_range != previous_range:
		change_range()
		previous_range = vision_range
