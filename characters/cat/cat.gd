extends Area

enum STATES {AWARE, DISTRACTED}
var state = STATES.AWARE

func _on_Area_body_entered(body: Node) -> void:
	if body.is_in_group("player"):
		if state == STATES.AWARE:
			if body.is_carring_ball():
				body.throw_ball()
				play_with_ball()
				EventsMgr.emit_signal('play_requested', 'Cat', 'Play')
			else:
				if body.is_hidden():
					body.unhide()
					EventsMgr.emit_signal('play_requested', 'Cat', 'Attk')

func play_with_ball():
	$AnimationPlayer.play("play_with_ball")
	state = STATES.DISTRACTED
