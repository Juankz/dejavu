extends Spatial

enum PICKUP_OBJECTS {BALL}
enum STATES {VISIBLE, HIDDEN}
var state = STATES.VISIBLE
var carring_objects = []

func hide():
	$AnimationPlayer.play("boxing")
	EventsMgr.emit_signal('play_requested', 'Player', 'Hide')
	state = STATES.HIDDEN

func unhide():
	$AnimationPlayer.play("unboxing")
	EventsMgr.emit_signal('play_requested', 'Player', 'Unhide')
	state = STATES.VISIBLE

func is_hidden():
	return state == STATES.HIDDEN

func pickup_ball():
	EventsMgr.emit_signal('play_requested', 'Player', 'Ball')
	carring_objects.append(PICKUP_OBJECTS.BALL)
	$ball.visible = true
	
func throw_ball():
	carring_objects.remove(PICKUP_OBJECTS.BALL)
	$ball.visible = false

func is_carring_ball():
	return carring_objects.has(PICKUP_OBJECTS.BALL)

func fall():
	var tween = get_node("Tween")
	tween.interpolate_property($MeshWrapper, "translation",
			$MeshWrapper.translation, $MeshWrapper.translation + Vector3(0, -5, 0), 0.5,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()


func _on_Tween_tween_all_completed() -> void:
	get_tree().call_group("game_manager", "_on_Restart_pressed")
