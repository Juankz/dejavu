extends Node

export var last_played_level: int = 1
var max_levels = 4
onready var current_level = $Level1.name

func _ready() -> void:
	get_tree().paused = true
	load_level(last_played_level)
#	load_level_data()
	
	#conecta eventos del AudioManager
	EventsMgr.emit_signal('play_requested', 'MX', 'MX_Main')
	EventsMgr.emit_signal('play_requested', 'BG', 'BG_City')
	EventsMgr.connect('stream_finished', self, '_on_stream_finished')
	
func load_level_data():
	pass

func game_over():
	get_tree().paused = true
	$GameOverMenu/Control.visible = true

func target_reached():
	EventsMgr.emit_signal('play_requested', 'UI', 'Portal')
	if last_played_level < max_levels:
		last_played_level = last_played_level + 1
		load_level(last_played_level)
	else:
		$EndScreen/Panel.visible = true
		$Tween.interpolate_property($EndScreen/Panel,"self_modulate", Color(1,1,1,0), Color(1,1,1,1),Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween.start()

func load_level(level: int) -> void:
	get_node(current_level).free()
	var NewLevel = load("res://Levels/Level%d.tscn" % level)
	var new_level = NewLevel.instance()
	(new_level as Node).pause_mode = PAUSE_MODE_STOP
	add_child(new_level)
	current_level = new_level.name
	if current_level == 'Level4':
		EventsMgr.emit_signal('stop_requested', 'BG', 'BG_City')
		EventsMgr.emit_signal('play_requested', 'BG', 'BG_Town')
		

func _on_Restart_pressed() -> void:
	EventsMgr.emit_signal('play_requested', 'UI', 'Retry')
	get_tree().paused = true
	load_level(last_played_level)
	$GameOverMenu/Control.visible = false
	toggle_pause_menu(false)


func _on_Play_pressed() -> void:
	EventsMgr.emit_signal('play_requested', 'UI', 'Click')
	$MainMenu/Control.visible = false
	$PauseMenu/Control.visible = true
	get_tree().paused = false


func _on_Pause_pressed() -> void:
	EventsMgr.emit_signal('play_requested', 'UI', 'Click')
	toggle_pause_menu(!get_tree().paused)

func toggle_pause_menu(value):
	get_tree().paused = value
	$PauseMenu/Control/Back.visible = value
	$PauseMenu/Control/Restart.visible = value


func _on_Back_pressed() -> void:
	EventsMgr.emit_signal('play_requested', 'UI', 'Back')
	$PauseMenu/Control.visible = false
	$PauseMenu/Control/Back.visible = false
	$PauseMenu/Control/Restart.visible = false
	$MainMenu/Control.visible = true
	$EndScreen/Panel.visible = false
	get_tree().paused = true
	load_level(last_played_level)

func _on_stream_finished(source, sound) -> void:
	if source == 'MX':
		yield(get_tree().create_timer(8), 'timeout')
		EventsMgr.emit_signal('play_requested', 'MX', 'MX_Main')
