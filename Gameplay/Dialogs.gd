tool
extends CanvasLayer

var fired = false
export var strings = PoolStringArray()

func _ready() -> void:
	if strings.size()>0:
		$Panel/MarginContainer/HBoxContainer/Label.text = strings[0]

func appeared():
	if !fired:
		$Timer.start()
		fired = true

func _on_Timer_timeout() -> void:
	$AnimationPlayer.play_backwards("appear")
