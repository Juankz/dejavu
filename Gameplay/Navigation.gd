extends GridMap

const ray_length = 500
const BREAKABLE_TILES = {
	"SEMIBROKEN": 9,
	"BROKEN": 10
}
const NOT_NAVIGATION_TILES = ['CityTileEmpty', 'TownTileEmpty', 'TownTileEmpty001', 'TownTileEmpty002']
const NAVIGATION_TILES = ['CityTileStraight', 'CityTileCorner', 'CityTileDeadEnd', 'CityTileT', 'CityTile4']
const map_to_world_offset = Vector3(0.0, -1, 0.0)
export(NodePath) var camera
export(NodePath) var player
var root_viewport: Viewport


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	camera = get_node(camera)
	player = get_node(player)
	root_viewport = get_tree().root

func _physics_process(delta: float) -> void:
	if Input.is_action_just_pressed('click'):
		var mouse_position = root_viewport.get_mouse_position()
		var from = camera.project_ray_origin(mouse_position)
		var to = from + camera.project_ray_normal(mouse_position) * ray_length
		var space_state = get_world().direct_space_state
		var selected = space_state.intersect_ray(from, to, [], collision_mask)
		if !selected.has('collider'):
			return
		if selected.collider is GridMap:
			var selected_coords = (selected.collider as GridMap).world_to_map(selected.position)
			var selected_cell = (selected.collider as GridMap).get_cell_item(selected_coords.x, selected_coords.y, selected_coords.z)
			var cell_item_name = ''
			if selected_cell == -1:
				# Sometimes the raycast fails to pick the proper cell by an offset of 1 unit
				# So we increase it and pick up the right cell
				selected_coords.y += 1
				selected_cell = (selected.collider as GridMap).get_cell_item(selected_coords.x, selected_coords.y, selected_coords.z)
				cell_item_name = (selected.collider as GridMap).mesh_library.get_item_name(selected_cell)
			else:
				cell_item_name = (selected.collider as GridMap).mesh_library.get_item_name(selected_cell)
			if !NOT_NAVIGATION_TILES.has(cell_item_name):
				if are_adjacents_cells(selected_coords, get_player_cell().grid_position):
					var where_to = map_to_world(selected_coords.x, selected_coords.y, selected_coords.z)
					where_to = Vector3(where_to.x, player.translation.y, where_to.z)
					(player as Spatial).get_node("MeshWrapper").look_at(where_to, Vector3.UP)
					player.rotation.y += PI
					player.translation = (selected.collider as GridMap).map_to_world(selected_coords.x, selected_coords.y, selected_coords.z) + map_to_world_offset
					EventsMgr.emit_signal('play_requested', 'Player', 'FS')
					
					if cell_item_name.find("planks") != -1:
						if cell_item_name.find("semibroken") != -1:
							self.set_cell_item(selected_coords.x,selected_coords.y,selected_coords.z, BREAKABLE_TILES.BROKEN)
							player.fall()
							EventsMgr.emit_signal('play_requested', 'Breakable', 'Wood_Break')
							EventsMgr.emit_signal('play_requested', 'Player', 'Scream')
						else:
							self.set_cell_item(selected_coords.x,selected_coords.y,selected_coords.z, BREAKABLE_TILES.SEMIBROKEN)
							EventsMgr.emit_signal('play_requested', 'Breakable', 'Wood_Damage')
						
func get_player_cell() -> Dictionary:
	var from = player.translation + Vector3(0, 0.1, 0)
	var to = from + Vector3(0,-5,0)
	return get_raycast_cell(from, to)
	
func get_raycast_cell(from: Vector3, to: Vector3) -> Dictionary:
	var space_state = get_world().direct_space_state
	var selected = space_state.intersect_ray(from, to)
	if !selected.has('collider'):
		return {}
	if selected.collider is GridMap:
		var selected_coords = (selected.collider as GridMap).world_to_map(selected.position)
		var selected_cell = (selected.collider as GridMap).get_cell_item(selected_coords.x, selected_coords.y, selected_coords.z)
		var cell_item_name = ''
		if selected_cell != -1:
			cell_item_name = (selected.collider as GridMap).mesh_library.get_item_name(selected_cell)
		else: 
			# Sometimes the raycast fails to pick the proper cell by an offset of 1 unit
			# So we increase it and pick up the right cell
			selected_coords.y += 1
			selected_cell = (selected.collider as GridMap).get_cell_item(selected_coords.x, selected_coords.y, selected_coords.z)
			cell_item_name = (selected.collider as GridMap).mesh_library.get_item_name(selected_cell)
		selected.grid_position = selected_coords
	return selected
	
func are_adjacents_cells(a: Vector3, b: Vector3) -> bool:
	var combinations = [Vector3.LEFT, Vector3.RIGHT, Vector3.FORWARD, Vector3.BACK]
	for combination in combinations:
		if a.is_equal_approx(b + combination):
			return true
	return false
