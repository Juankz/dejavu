extends Area

func _on_Target_body_entered(body: Node) -> void:
	if body.is_in_group("player"):
		get_tree().call_group("game_manager", "target_reached")
