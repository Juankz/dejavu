extends Camera

export(NodePath) var target
var dist: Vector3

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	target = (get_node(target) as Spatial)
	dist = self.translation - target.translation

func _process(delta: float) -> void:
	#TODO: add exponential as factor of lerp
	translation = translation.linear_interpolate(target.translation + dist, delta)
