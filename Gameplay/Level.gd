extends Spatial

var max_levels = 3

func _on_Target_body_entered(body: Node) -> void:
	if body.is_in_group("player"):
		var level: int = int(name.lstrip("Level")) + 1
		if level <= max_levels:
			get_tree().change_scene("res://Levels/Level%d.tscn" % level)
