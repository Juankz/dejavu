extends Area


func _on_Box_body_entered(body: Node) -> void:
	if body.is_in_group("player"):
		body.hide()
		self.queue_free()
