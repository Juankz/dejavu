extends Area

func _on_Ball_body_entered(body: Node) -> void:
	if body.is_in_group("player"):
		body.pickup_ball()
		self.queue_free()
